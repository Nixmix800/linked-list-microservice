'use strict';

const listController = require('./listController');
let bodyParser = require("body-parser");

let jsonParser = bodyParser.json();

module.exports = function(app) {

    app.route('/about')
        .get(listController.about);

    app.route('/linkedlist/:numberofitems')
        .get(listController.generateLinkedList);

    app.route('/linkedlist')
        .post(jsonParser, listController.reverseLinkedList)

    app.route('/linkedlist')
        .get(listController.getLinkedLists)
};