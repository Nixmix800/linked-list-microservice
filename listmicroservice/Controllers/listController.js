'use strict';

const properties = require ('../package.json')
const list = require('../Services/listLogic');

const controllers = {
    about: function (req, res) {
        const aboutInfo = {
            name: properties.name,
            version: properties.version,
            endpoints: [
                {
                    url: '/linkedlist/x',
                    method: 'GET',
                    description: 'Generates a list that has x items with random numbers between 0 and 100 for the data.' +
                        'The list is then stored inside the mongodb datastore.',
                    params: {
                        name: 'x',
                        description: 'Number of items the list should have',
                        limitation: 'A maximum of 6170 items can be created'
                    }
                },
                {
                    url: '/linkedlist',
                    method: 'POST',
                    description: 'Takes a linked list in json format and reverses it.'
                },
                {
                    url: '/linkedlist',
                    method: 'GET',
                    description: 'Reads all linked lists from mongodb datastore and outputs them.'
                }]
        }
        res.json(aboutInfo);
    },
    generateLinkedList: function (req, res) {
        list.generate(req, res, function (err, linkedList) {
            if (err)
                res.send(err);
            res.json(linkedList);
        });

    },
    reverseLinkedList: function (req, res) {
        list.reverse(req, res, function (err, linkedList) {
            if (err)
                res.send(err);
            res.json(linkedList);
        });
    },
    getLinkedLists: function (req, res) {
        list.getAll(req, res, function (err, linkedLists) {
            if (err)
                res.send(err);
            res.json(linkedLists);
        })
    }
};

module.exports = controllers;