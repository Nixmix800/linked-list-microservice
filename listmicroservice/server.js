"use strict";

const express = require('express')
const server = express();
const port = process.env.PORT || 3000; //set port to env-var $PORT or 3000 if env is not specified

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const routes = require('./Controllers/routes');
routes(server);

server.use(express.json({
    type: "*/*"
}));

server.use('/api-docs', swaggerUi.serve);
server.get('/api-docs', swaggerUi.setup(swaggerDocument));

server.listen(port, function () {
    console.log('Server started on port: ' + port)
})

//mongoose connection:
const mongoose = require("mongoose");

const connString = process.env.MONGODB_CONNSTRING ?? "mongodb://node:Test123@localhost";

mongoose.connect(connString,
    {
        useNewUrlParser: true,

        useUnifiedTopology: true
    }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error: "));
db.once("open", function () {
    console.log("Connected successfully");
});
