class ListNode {
    constructor (data) {
        this.data = data;
        this.nextNode = null;
    }
}

module.exports = ListNode