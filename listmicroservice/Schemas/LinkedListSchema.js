const mongoose = require("mongoose");

const LinkedListSchema = new mongoose.Schema({
    linkedList:
        {
            type: mongoose.Schema.Types.Mixed
        }
})

//let linkedList = new LinkedListSchema;

const LinkedListModel = mongoose.model("LinkedList", LinkedListSchema);

//linkedList.listNode = new LinkedList()

module.exports = LinkedListModel;