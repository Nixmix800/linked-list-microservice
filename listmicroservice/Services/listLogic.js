'use strict';

const ListNode = require("../Classes/ListNode");
const LinkedList = require("../Classes/LinkedList");
const LinkedListModel = require('../Schemas/LinkedListSchema');

const maxNumber = 100;

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

let listLogic = {
    generate: function (req, res, next) {

        let numItems = req.params.numberofitems;
        let linkedList = new LinkedList();

        if(numItems !== 0) {
            linkedList = new LinkedList(new ListNode(getRandomInt(maxNumber)))
            let currentNode = linkedList.head;

            if(numItems > 6170) {
                numItems = 6170
            }

            for(let i = 1; i < numItems; i++) {
                currentNode.nextNode = new ListNode(getRandomInt(maxNumber))
                currentNode = currentNode.nextNode;
            }
        }

        linkedList.length = numItems;

        let linkedListModel = new LinkedListModel({linkedList: linkedList});

        try {
            linkedListModel.save();
            res.json(linkedList);
        } catch (error) {
            res.status(500).send(error);
        }
    },
    reverse: function (req, res, next) {
        let linkedList = req.body
        let currentNode = linkedList.head
        let prevNode = null
        let nextNode = currentNode.nextNode;

        while(currentNode.nextNode != null) {
            //reverse
            nextNode = currentNode.nextNode;
            currentNode.nextNode = prevNode;

            //setup for next iter
            prevNode = currentNode;
            currentNode = nextNode;
        }

        currentNode.nextNode = prevNode;

        linkedList.head = currentNode;

        let linkedListModel = new LinkedListModel ({linkedList: linkedList});
        linkedListModel.save();

        try {
            res.json(linkedList);
        } catch (error) {
            res.status(500).send(error);
        }
    },
    getAll: function (req, res, next) {

        let listResults = [];

        LinkedListModel.find({},null,{lean: true}, function(err, results)
        {
            listResults = results;

            try {

                res.json(listResults);
            } catch (error) {
                res.status(500).send(error);
            }
        });
    }
}

module.exports = listLogic;