# Linked List Microservice

## Name
Linked List Microservice

## Description
This is a microservice that reverses a linked list. It is based on a node.js application which stores the results in a mongodb Database.

It has the ability to generate a linked list with a given amount of elements and stores it in a mongodb database.
This list can then be taken and sent back to the microservice to be reveresed.
All lists that are stored inside the database can be read.

## Installation
To install this microservice, run `docker-compose up` in the root directory of the microservice.

## Usage
Call [Swagger UI](http://localhost:8080/api-docs) for further information.
All Endpoints and their objects are documented there.

### Endpoints

- [/api-docs](http://localhost:8080/api-docs)
- [/linkedlist/.numberofitems](http://localhost:8080/linkedlist/10)
- [/linkedlist (GET)](http://localhost:8080/linkedlist)
- [/linkedlist (POST)](http://localhost:8080/linkedlist)
