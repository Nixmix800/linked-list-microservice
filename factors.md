# Linked List Microservice

This is a microservice that reverses a linked list

# 12 Factors beeing used

1) Codebase
    * Code is tracked in GitLab VCS
2) Dependencies
    * All dependencies are installed via docker-compose (except for docker itself)
3) Config
    * Config is stored in the properties file of the nodejs app
4) Backing services
    * MongoDB is created and initialized via docker-compose and nodejs app
5) Build, release, run _(NOT FULFILLED)_
    * There is no CD configured. I would add a step after the creation of the docker images to roll out these images on a host, where docker is installed (To be honest: I would write helm charts and deploy the app in a k8s-cluster)
6) Processes
    * Services are run in different docker containers (nodejs and mongodb)
7) Port binding
    * Port binding is configured in docker-compose file
8) Concurrency _(NOT FULFILLED)_
    * Is not implemented. I would use k8s with a loadbalancer to achieve this.
9) Disposability _(NOT FULFILLED)_
    * Such things as Blue-Green-Deployments are not set up. Once again, I would use k8s for this.
10) Dev/prod parity
    * There is no difference between dev and prod except for the different ports the service runs on.
11) Logs _(NOT FULFILLED)_
    * Logs are not configured due to time constraints.
12) Admin process _(NOT FULFILLED)_
    * This application invludes no such processes.
